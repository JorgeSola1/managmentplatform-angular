import { TestBed } from '@angular/core/testing';

import { PanelMenuItemsService } from './panel-menu-items.service';

describe('PanelMenuItemsService', () => {
  let service: PanelMenuItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PanelMenuItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
