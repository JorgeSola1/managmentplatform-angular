import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PanelMenuItemsService {

private features = new BehaviorSubject('');
currentFeatures = this.features.asObservable() 

private items = new BehaviorSubject([{label:"Home"}]);
currentItem = this.items.asObservable()

constructor() {}

changeFeature(feature: any): void {
  //console.log("Feature changed:", feature)
  const newValue = feature.item.label;
  this.features.next(newValue); 
}

changeItems(item: any): void {
  //console.log("Feature changed:", feature)
  this.items.next(item); 
}
}
