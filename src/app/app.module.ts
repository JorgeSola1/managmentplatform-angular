import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BreadcrumbComponent } from './components/home/breadcrumb/breadcrumb.component';
import { PanelMenuComponent } from './components/home/panel-menu/panel-menu.component';
import { LoginComponent } from './components/login/login.component';
import { TenantsComponent } from './components/platformManagment/tenants/table/tenants.component';

import {BreadcrumbModule} from 'primeng/breadcrumb';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuModule } from 'primeng/menu';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { BasicInformationComponent } from './components/platformManagment/tenants/add/basic-information/basic-information.component';
import { ConfigurationComponent } from './components/platformManagment/tenants/add/configuration/configuration.component';
import { AddComponent } from './components/platformManagment/tenants/add/add.component';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { EditComponent } from './components/platformManagment/tenants/edit/edit.component';
import { EditconfigurationComponent } from './components/platformManagment/tenants/edit/editconfiguration/editconfiguration.component';
import { EditBasicInformationComponent } from './components/platformManagment/tenants/edit/edit-basic-information/edit-basic-information.component';


@NgModule({
  declarations: [
    AppComponent,
    BreadcrumbComponent,
    PanelMenuComponent,
    LoginComponent,
    TenantsComponent,
    BasicInformationComponent,
    ConfigurationComponent,
    AddComponent,
    EditComponent,
    EditconfigurationComponent,
    EditBasicInformationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BreadcrumbModule,
    BrowserAnimationsModule,
    MenuModule,
    PanelMenuModule,
    ButtonModule,
    TableModule,
    DynamicDialogModule,
    InputTextModule,
    InputTextareaModule,
    FormsModule,
    ConfirmDialogModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
