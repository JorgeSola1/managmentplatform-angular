import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PanelMenuItemsService } from '../../../services/panel-menu-items.service'
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  home:MenuItem;
  features:String;
  items: any;
  
  subscriptionFeature: Subscription;
  subscriptionItems: Subscription;

  constructor(private managment: PanelMenuItemsService) { }

  
  ngOnInit(): void {

    this.subscriptionItems = this.managment.currentItem.subscribe(items => this.items = items)
    this.subscriptionFeature = this.managment.currentFeatures.subscribe(features => this.features = features);
    this.home = {icon: 'pi pi-home', routerLink: '/'};  
  }

  restart(){
    this.items=[{label:"Home"}]
    this.managment.changeItems(this.items)
    console.log("HEY",this.items)
  }

}
