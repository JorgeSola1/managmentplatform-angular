import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PanelMenuItemsService } from '..//../../services/panel-menu-items.service';

@Component({
  selector: 'app-panel-menu',
  templateUrl: './panel-menu.component.html',
  styleUrls: ['./panel-menu.component.scss']
})
export class PanelMenuComponent implements OnInit {

  features:String;
  items:any;

  subscriptionFeature: Subscription;
  subscriptionItems: Subscription;

  constructor(private managment: PanelMenuItemsService) {}

  platform: any = [
    {label:"Platform Managment", command: (event) => {this.managment.changeFeature(event);this.selectPage()}, 
    items:[
      {label:"Tenants", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Clusters", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Applications", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Platform Service", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Domain Service", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Domain Service Version", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"External Applications", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"External Applications Subscriptions", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"WS-Topics",command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Subscription to WS-Topics",command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Registration of Publishers",command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Message Broke Topics",command: (event) => {this.managment.changeFeature(event); this.selectPage()}}]},

    {label:"Service Management", command: (event) => {this.managment.changeFeature(event);this.selectPage()},
    items:[
      {label:"Message Cache", command: (event) => {this.managment.changeFeature(event); this.selectPage()}}]},

    {label:"Product Managment",command: (event) => {this.managment.changeFeature(event);this.selectPage()},
    items:[
      {label:"ArgoCD Web Console", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"GitLab Web Console", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Harbor Web Console", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"Kubana Dashboard", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"WSO2 Identity Servery Web Console", command: (event) => {this.managment.changeFeature(event); this.selectPage()}},
      {label:"CA Identity Suite Web Console", command: (event) => {this.managment.changeFeature(event); this.selectPage()}}]}]

      
  ngOnInit(): void {
    this.subscriptionFeature = this.managment.currentFeatures.subscribe(features => this.features = features);
    this.subscriptionItems = this.managment.currentItem.subscribe(items => this.items = items)
  }

  selectPage(){
    
    console.log("START SELECT PAGE")
    console.log("Feature:", this.features)
    var dict = [];
    dict.push({label:"Home"})

    if(this.features.length != 0){
      for(let platform of this.platform){
        if(platform.label == this.features){
          dict.push({
            label:platform.label})
        }
        for(let item of platform.items){
          if(this.features == item.label){
            dict.push({label:platform.label})
            dict.push({label:item.label})
         }}
    }}
    console.log("ITEMS:", dict);
    this.items = dict;
    this.managment.changeItems(this.items)
}
}