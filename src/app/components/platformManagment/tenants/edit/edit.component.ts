import { Component, OnInit } from '@angular/core';
import {DynamicDialogConfig} from 'primeng/dynamicdialog';
import { EntitiesService } from './../services/entities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  product:any;

  item:any
  itemSubscription: Subscription;

  entities:any[]
  subscriptionEntities: Subscription;

  constructor(private dynamicDialogConfig: DynamicDialogConfig,
              private entitie: EntitiesService){}
  
  ngOnInit(): void{

    this.itemSubscription = this.entitie.currentItem.subscribe(item => this.item = item)
    this.product =  this.dynamicDialogConfig.data.product;
    this.item = this.product;

    this.itemSubscription = this.entitie.currentItem.subscribe(item => this.item = item);
    this.subscriptionEntities = this.entitie.currentEntities.subscribe(entities => this.entities = entities)
    
    console.log("Initial edit entities:",this.entities)
  }
}
