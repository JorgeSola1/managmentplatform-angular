import { Component, OnInit } from '@angular/core';
import { PanelMenuItemsService } from '../../../../../services/panel-menu-items.service';
import { EntitiesService } from '../../services/entities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-basic-information',
  templateUrl: './edit-basic-information.component.html',
  styleUrls: ['./edit-basic-information.component.scss']
})
export class EditBasicInformationComponent implements OnInit {

  item:any;
  itemSubscription: Subscription;

  entities:any[]
  subscriptionEntities: Subscription;

  features:any;
  subscriptionFeature: Subscription;

  name:String;
  subscriptionName: Subscription;

  description:String;
  subscriptionDescription: Subscription;

  id:any;
  subscriptionId: Subscription;

  constructor(private managment: PanelMenuItemsService,
              private entitie: EntitiesService) { }

  ngOnInit(): void {

    

    this.itemSubscription = this.entitie.currentItem.subscribe(item => this.item = item);
    this.subscriptionEntities = this.entitie.currentEntities.subscribe(entities => this.entities = entities)
    this.subscriptionName = this.entitie.currentName.subscribe(name => this.name = name);
    this.subscriptionDescription = this.entitie.currentDescription.subscribe(description => this.description = description);
    this.subscriptionFeature = this.managment.currentFeatures.subscribe(features => this.features = features);
    this.subscriptionId = this.entitie.currentId.subscribe(id => this.id = id)

    console.log("Basic information item:", this.item);

    this.id = this.item.id;
    this.name = this.item.name;
    this.description = this.item.description;

  }

  newName(){
    this.entitie.changeName(this.name)
  }

  newDescription(){
    this.entitie.changeDescription(this.description)
  }

}
