import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditconfigurationComponent } from './editconfiguration.component';

describe('EditconfigurationComponent', () => {
  let component: EditconfigurationComponent;
  let fixture: ComponentFixture<EditconfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditconfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditconfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
