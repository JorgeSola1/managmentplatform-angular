import { Component, Input, OnInit } from '@angular/core';
import { PanelMenuItemsService } from '../../../../../services/panel-menu-items.service';
import { EntitiesService } from '../../services/entities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-editconfiguration',
  templateUrl: './editconfiguration.component.html',
  styleUrls: ['./editconfiguration.component.scss']
})
export class EditconfigurationComponent implements OnInit {

  item:any;
  itemSubscription: Subscription;

  entities:any[]
  subscriptionEntities: Subscription;

  features:any;
  subscriptionFeature: Subscription;

  name:String;
  subscriptionName: Subscription;

  description:String;
  subscriptionDescription: Subscription;

  id:any;
  subscriptionId: Subscription;

  cpu:any;
  subscriptionCpu: Subscription;

  memory:any;
  subscriptionMemory: Subscription;

  storage:any;
  subscriptionStorage: Subscription;

  constructor(private managment: PanelMenuItemsService,
              private entitie: EntitiesService) { }

  ngOnInit(): void {


    this.itemSubscription = this.entitie.currentItem.subscribe(item => this.item = item);
    this.subscriptionEntities = this.entitie.currentEntities.subscribe(entities => this.entities = entities)
    this.subscriptionName = this.entitie.currentName.subscribe(name => this.name = name);
    this.subscriptionDescription = this.entitie.currentDescription.subscribe(description => this.description = description);
    this.subscriptionFeature = this.managment.currentFeatures.subscribe(features => this.features = features);
    this.subscriptionId = this.entitie.currentId.subscribe(id => this.id = id)
    this.subscriptionCpu = this.entitie.currentCpu.subscribe(cpu => this.cpu = cpu);
    this.subscriptionMemory = this.entitie.currentMemory.subscribe(memory => this.memory = memory);
    this.subscriptionStorage = this.entitie.currentStorage.subscribe(storage => this.storage = storage);

    console.log("Configuration information item:", this.item);

    this.cpu = this.item.cpu;
    this.memory = this.item.memory;
    this.storage = this.item.storage;

  }

  changeCpu(){
    this.entitie.changeCpu(this.cpu)
  }

  changeMemory(){
    this.entitie.changeMemory(this.memory)
  }

  changeStorage(){
    this.entitie.changeStorage(this.storage)
  }
  
  update(){
    
    var dict = [{id:this.id, name:this.name, description:this.description, cpu:this.cpu, memory:this.memory, storage: this.storage}]
    console.log("DICT:", dict)
    var newId = parseInt(this.id)-1
    console.log("old items:",this.entities)
    this.entities.splice(newId, 1, dict)
    this.entitie.changeEntities(this.entities)
    console.log("new items:",this.entities)
  }

}
