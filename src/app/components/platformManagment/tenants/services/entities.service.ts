import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EntitiesService {

  private item = new BehaviorSubject([]);
  currentItem = this.item.asObservable() 

  private cpu = new BehaviorSubject('');
  currentCpu = this.cpu.asObservable() 

  private memory = new BehaviorSubject('');
  currentMemory = this.memory.asObservable() 

  private storage = new BehaviorSubject('');
  currentStorage = this.storage.asObservable() 

  private id = new BehaviorSubject('');
  currentId = this.id.asObservable()

  private entities = new BehaviorSubject([]);
  currentEntities = this.entities.asObservable() 

  private name = new BehaviorSubject('');
  currentName = this.name.asObservable() 

  private description = new BehaviorSubject('');
  currentDescription = this.description.asObservable() 

  constructor() {}

changeItem(item: any): void {
  this.item.next(item); 
}

changeEntities(entities: any): void {
    this.entities.next(entities); 
}

changeName(name: any): void {
  this.name.next(name); 
}

changeDescription(description: any): void {
  this.description.next(description); 
}

changeId(id:any): void {
  this.id.next(id); 
}

changeCpu(cpu:any): void {
  this.cpu.next(cpu); 
}

changeMemory(memory:any): void {
  this.memory.next(memory); 
}

changeStorage(storage:any): void {
  this.storage.next(storage); 
}
}
