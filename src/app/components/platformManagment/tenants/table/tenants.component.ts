import { Component, OnInit } from '@angular/core';
import { SortEvent } from 'primeng/api';
import { Subscription } from 'rxjs';
import { PanelMenuItemsService } from '../../../../services/panel-menu-items.service';
import { EntitiesService } from '../services/entities.service';
import { DialogService } from 'primeng/dynamicdialog';
import { AddComponent } from '../add/add.component';
import { EditComponent } from '../edit/edit.component';
import { ConfirmationService, ConfirmEventType, MessageService } from 'primeng/api';




@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.scss'],
  providers:[DialogService,ConfirmationService,MessageService]
})
export class TenantsComponent implements OnInit {

  features:String;
  subscriptionFeature: Subscription;

  entities:any[];
  subscriptionEntities: Subscription;

  items:any[];
  subscriptioItems: Subscription;

  cols: any[];

  constructor(private managment: PanelMenuItemsService,
              private entitie: EntitiesService,
              private dialogService: DialogService,
              private confirmationService: ConfirmationService, 
              private messageService: MessageService){ }

  ngOnInit(): void {

    this.subscriptionFeature = this.managment.currentFeatures.subscribe(features => this.features = features);
    this.subscriptionEntities = this.entitie.currentEntities.subscribe(entities => this.entities = entities);
    this.subscriptioItems = this.managment.currentItem.subscribe(items => this.items = items);


    this.cols = [
      {field:'Id', header:'Id'},
      {field:'Name', header:'Name'},
      {field:'Description', header:'Description'},
      {field:'Actions', header:'Actions'},

    ]
  }

  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
        let value1 = data1[event.field];
        let value2 = data2[event.field];
        let result = null;

        if (value1 == null && value2 != null)
            result = -1;
        else if (value1 != null && value2 == null)
            result = 1;
        else if (value1 == null && value2 == null)
            result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
            result = value1.localeCompare(value2);
        else
            result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.order * result);
    });

  }

  cancel(){
    this.entitie.changeId("");
    this.entitie.changeName("");
    this.entitie.changeDescription("")
    this.entitie.changeCpu("");
    this.entitie.changeStorage("");
    this.entitie.changeMemory("");
  }


  showDynamicDialogAdd():void{
    this.cancel();
    const ref = this.dialogService.open(AddComponent ,{
      width:"60%"
    }
    )}

  showDynamicDialogEdit(product:any[]):void{
    const ref = this.dialogService.open(EditComponent ,{
      data:{
        product:product
      },
      width:"60%"
    }
    )}

  delete(row){
    this.entities.splice(row-1,1);
    this.entitie.changeEntities(this.entities)
  }

  confirm(row) {
    this.confirmationService.confirm({
    message: 'Are you sure that you want to proceed?',
    header: 'Confirmation',
    icon: 'pi pi-exclamation-triangle',
    accept: () => {
        console.log("Petición aceptada")
        this.messageService.add({severity:'info', summary:'Confirmed', detail:'You have accepted'});
        this.delete(row)
      },
    reject: (type) => {
        switch(type) {
            case ConfirmEventType.REJECT:
                this.messageService.add({severity:'error', summary:'Rejected', detail:'You have rejected'});
            break;
            case ConfirmEventType.CANCEL:
                this.messageService.add({severity:'warn', summary:'Cancelled', detail:'You have cancelled'});
            break;
        }
    }
});
  }
}

