import { Component, OnInit } from '@angular/core';
import { PanelMenuItemsService } from '../../../../../services/panel-menu-items.service';
import { EntitiesService } from '../../services/entities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-basic-information',
  templateUrl: './basic-information.component.html',
  styleUrls: ['./basic-information.component.scss']
})
export class BasicInformationComponent implements OnInit {

  features:String;
  subscriptionFeature: Subscription;

  name:String;
  subscriptionName: Subscription;

  description:String;
  subscriptionDescription: Subscription;

  constructor(private managment: PanelMenuItemsService,
              private entities: EntitiesService) { }

  ngOnInit(): void {

    this.subscriptionName = this.entities.currentName.subscribe(name => this.name = name);
    this.subscriptionDescription = this.entities.currentDescription.subscribe(description => this.description = description);
    this.subscriptionFeature = this.managment.currentFeatures.subscribe(features => this.features = features);

  }

  newName(){
    this.entities.changeName(this.name)
  }

  newDescription(){
    this.entities.changeDescription(this.description)
  }
}
