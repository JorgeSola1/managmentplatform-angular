import { Component, Input, OnInit } from '@angular/core';
import { PanelMenuItemsService } from '../../../../../services/panel-menu-items.service';
import { EntitiesService } from '../../services/entities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  features:String;
  subscriptionFeature: Subscription;

  name:String;
  subscriptionName: Subscription;

  description:String;
  subscriptionDescription: Subscription;

  entities:any[];
  subscriptionEntities: Subscription;

  id:String;
  subscriptionId: Subscription;

  cpu:String;
  subscriptionCpu: Subscription;

  memory:String;
  subscriptionMemory: Subscription;

  storage:String;
  subscriptionStorage: Subscription;

  constructor(private managment: PanelMenuItemsService,
              private entitie: EntitiesService) { }

  ngOnInit(): void {
    
    this.subscriptionEntities = this.entitie.currentEntities.subscribe(entities => this.entities = entities);
    this.subscriptionName = this.entitie.currentName.subscribe(name => this.name = name);
    this.subscriptionDescription = this.entitie.currentDescription.subscribe(description => this.description = description);
    this.subscriptionId = this.entitie.currentId.subscribe(id => this.id = id)
    this.subscriptionCpu = this.entitie.currentCpu.subscribe(cpu => this.cpu = cpu);
    this.subscriptionMemory = this.entitie.currentMemory.subscribe(memory => this.memory = memory);
    this.subscriptionStorage = this.entitie.currentStorage.subscribe(storage => this.storage = storage);
    
    this.subscriptionFeature = this.managment.currentFeatures.subscribe(features => this.features = features);


    }

    create(){
      console.log(this.entities)
      var newId = 1;
      if(this.entities.length > 0){
        var newId = parseInt(this.entities[this.entities.length-1].id) + 1
      }
      this.entities.push({id:newId, name:this.name, description:this.description, cpu:this.cpu, memory:this.memory,storage:this.storage})
      this.entitie.changeEntities(this.entities)

      console.log("Items:", this.entities)

    }
}

